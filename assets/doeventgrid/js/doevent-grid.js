(function (window, document) {
    // Modules
    var dRow = angular.module('dRow',[]);
    dRow.factory('objRow',function($rootScope){
        var objRow = [];
        var objRowService = {};
        var styleth = [];
        
        objRowService.getStyle = function(){
            return styleth;
        };
        
        objRowService.setStyle = function(st){
            styleth = st;
        };

        objRowService.getObjRow = function (){
            return objRow;
        };

        objRowService.setObjRow = function (data){
            objRow = data;
        };

        objRowService.setObjRowId  = function (tyid){
            objRowId = tyid;
        };

        objRowService.getObjRow  = function (){
            return objRow;
        };

        return objRowService;
    });
    
    dRow.directive('dRow', function (objRow, eventGrid) {
        return {
            restrict: 'A',
            scope : {option:'='},
            link: function(scope, elem, attrs) {
                //set width 
                var stl = objRow.getStyle();
                var tdgrid = elem.find('div.td');
                angular.forEach(tdgrid, function(value, key){
                    var td = elem.find(value);
                    td.attr('style',stl[key]);                    
                });
                
                elem.bind('click',function(){
                    console.log(scope);
                    eventGrid.findRecord(attrs.tabindex);
                    elem.addClass("dselect");                    
                });

                elem.bind('keyup', function (event) {
                    switch (event.which){
                        case 38:
                            if (!event.ctrlKey) {
                                if (elem.prev().html() !== null) {
                                    elem.prev().focus();
                                    elem.prev().addClass("dselect");
                                    elem.removeClass("dselect");
                                    //elem[0].previousElementSibling.focus();
                                    eventGrid.findRecord(eval(attrs.tabindex + "-1"));
                                }
                            }
                            break;
                        case 40:
                            if (!event.ctrlKey) {
                                if (elem.next().html() !== null) {
                                    elem.next().focus();
                                    elem.next().addClass("dselect");
                                    elem.removeClass("dselect");
                                    //elem[0].nextElementSibling.focus();
                                    eventGrid.findRecord(eval(attrs.tabindex + "+1"));
                                }
                            }
                            break;
                        /*case 69:                            
                            var btn = elem.find('a.btn-success');
                            btn.click();
                            return false;
                            break;
                        case 68:                            
                            var btn = elem.find('a.btn-danger');
                            btn.click();
                            return false;
                            break;*/
                    }
                    
                });
            }
        };
    });
    
    var dCol = angular.module('dCol',[]);
    dCol.directive('dCol',function(objRow){
        return {
            restrict : 'A',
            link : function(scope, elem, attrs){
                
            }
        };
    });
    
    var dRecFind = angular.module('dRecFind',[]);
    dRecFind.directive('dCol',function(objRow){
        return {
            restrict : 'A',
            link : function(scope, elem, attrs){
                
            }
        };
    });
    
    var dHead = angular.module('dHead',[]);
    dHead.directive('dHead',function(objRow){
        return {
            restrict : 'A',
            link : function(scope, elem, attrs){
                elem.bind("scroll", function() {
                    
                });
            }
        };
    });
    
    
    var doeventGrid = angular.module('dGrid',['dRow','dCol']);
    doeventGrid.factory('eventGrid',function($rootScope, $http, $filter){
        //Url CRUD
        var urlInsert = '';
        var urlEdit = '';
        var urlDelete = '';

        var urlPager = '';
        var index = 0;
        var records =  [];
        var record = [];
        var eventGridService = {};

        //Untuk Pagging
        var stop = 20;
        var max_page = 0;
        var nextpage = 0;

        //Untuk sorting data
        var fieldOrder = '';
        var statusSort = false;

        function search(varFind){
            var fieldcari = '';
            var fieldvalue = '';
            angular.forEach(varFind, function(value, key){
                if (key!=='') fieldcari += ((fieldcari!=='')?';':'')+ key ;
                if(value!=='') fieldvalue += ((fieldvalue!=='')?';':'')+ value ;
            });
            
            $http.get(urlPager, {
                params: {
                    'field': (varFind!=='')?fieldcari:'',
                    'value': (varFind!=='')?fieldvalue:'',
                    'stop': nextpage
                }
            }).
            success(function(data) {
                if (data !== undefined) {
                    records = data.rec;
                    max_page = data.max_page;
                    if (max_page < nextpage) {
                        nextpage = 0;
                    }
                }
            });
        }

        eventGridService.setUrlPager = function(dt){
            urlPager = dt;
        };

        eventGridService.setSearch = function(varFind){
            search(varFind);
        };       
        
        /*
        * Untuk pagging dan pengaolahan data pada record data
        */
        
        eventGridService.insertRecords = function (dt){
            record = dt;
            records.push(record);
        };
        
        eventGridService.editRecords = function (){
            records[index] = record;
        };
        
        eventGridService.deleteRecords = function (){
            records.splice(index, 1);
        };
        
        eventGridService.setRecords = function (data){
            records = data;
        };

        eventGridService.getRecords = function (){
            return records ;
        };

        eventGridService.findRecord = function (idx){
            index = idx;
            record = records[idx];
        };

        eventGridService.getRecord = function (){
            return record ;
        };

        eventGridService.refreshData = function(){
            search('');
        };
        
        eventGridService.setStop = function(dt){
            stop = dt;
        };

        eventGridService.setMaxPage = function(dt){
            max_page = dt;
        };

        eventGridService.setNextPage = function(dt){
            nextpage = dt;
        };

        eventGridService.setFirstPages = function(varFind){
            if(urlPager!==''){
                if (nextpage > 0) {
                    nextpage = 0;
                    search(varFind);
                }
            }
        };
        
        eventGridService.setPrevPages = function(varFind){
            if(urlPager!==''){
                if (nextpage > 0) {
                    nextpage = nextpage - stop;
                    search(varFind);
                }
            }
        };
        
        eventGridService.setNextPages = function(varFind){
            
            if(urlPager!==''){
                if ((nextpage + stop) <= max_page) {
                    nextpage = nextpage + stop;
                    search(varFind);
                }
            }
        };
        
        eventGridService.setLastPages = function(varFind){
            if(urlPager!==''){
                if (nextpage < max_page) {
                    nextpage = max_page;
                    search(varFind);
                }
            }
        };
        
        eventGridService.getTitlePages = function(){
            return 'Page '+ eval ( nextpage + "/" + stop + "+1" ) +  ' of ' + eval(  max_page + "/" + stop + "+1");
        };
        
        return eventGridService;
    });

    doeventGrid.directive('dGrid',function(eventGrid, objRow){
        return{
            restrict : 'A',
            scope : {option:'=dGrid'},
            link : function(scope, elem, attrs){
                //console.log(elem.width());
                var lebar = elem.width();
                var op = scope.option;
                //console.log(op);
                eventGrid.setStop(op.stop);
                eventGrid.setMaxPage(op.maxpage);
                eventGrid.setNextPage(op.nextpage);
                eventGrid.setUrlPager(op.urlPager);
                
                if ( op.urlPager !=='' ){
                    eventGrid.refreshData();
                }
                
                var grid = elem.find('div.dGrid');
                var scrl = elem.find('div.setscroll');
                var tbody = elem.find('div.tbody');
                var tfoot = elem.find('div.tfoot');
                var thgrid = elem.find('div.th');
                
                elem.attr('style','width:' + lebar + 'px;margin-left:0px; margin-top:5px;');
                scrl.attr('style','width:' + lebar + 'px;');
                tbody.attr('style','width:' + lebar + 'px;');
                tfoot.attr('style','width:' + lebar + 'px;');
                grid.attr('style','width:' + lebar + 'px;');

                var styleth = [];
                angular.forEach(thgrid, function(value, key){
                    var th = elem.find(value);
                    styleth[key] = th.attr('style');
                });
                
                objRow.setStyle(styleth);
                
                //On scrol 
                /*var scl = elem.find('.setscroll');
                scl.bind("scroll", function(obj) {
                    //console.log(scl[0].scrollTop);
                    var thead = elem.find('div.thead');
                    if(scl[0].scrollTop>0){
                        if (thead.attr('style')=='' ){
                            thead.attr('style','display:fixed;');
                            var tbody = elem.find('div.tbody');
                            
                        }
                    }else{
                        if (thead.attr('style')!='' ){
                            thead.attr('style','');
                        }
                    }
                });*/
            }
        };
    });
})(window, document);